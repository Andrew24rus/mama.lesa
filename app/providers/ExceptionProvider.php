<?php


namespace ADFM\Providers;


use ADFM\exceptions\ErrorExceptions;

class ExceptionProvider extends ServiceProvider
{

    public function register()
    {
        // Error Handlers
        $this->container['errorHandler'] =
        $this->container['phpErrorHandler'] = function ($container) {
            return new ErrorExceptions($container);
        };

        // Not Found Handler
        /*$this->container['notFoundHandler'] =
        $this->container['notAllowedHandler'] = function ($ci) {
            return function ($request, $response) use ($ci) {
                return $ci->view->render($response->withStatus(404), '404.html.twig');
            };
        };*/
    }

    public function boot()
    {
    }
}