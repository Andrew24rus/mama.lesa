<?php


namespace ADFM\Providers;


use Slim\Container;

abstract class ServiceProvider
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    abstract public function register();

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    abstract public function boot();
}