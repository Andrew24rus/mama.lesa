<?php


namespace ADFM\Providers;


use Slim\Container;

class RouteProvider extends ServiceProvider
{
    private $dir;

    private $routers = [
        'api',
        'public',
    ];

    public function __construct(Container $container)
    {
        parent::__construct($container);

        $this->dir = APP_PATH . 'routes';
    }

    public function register()
    {
    }

    public function boot()
    {
        foreach ($this->routers as $router) {
            include_once "{$this->dir}/{$router}.php";
        }
    }
}