<?php


namespace ADFM\Controller\Traits;


use ADFM\Order\Product\Cake\BiscuitCake;
use ADFM\Order\Product\Cake\Cake;
use ADFM\Order\Product\Cake\MousseCake;

trait CakeByType
{
    private function createCakeByType(string $type): Cake
    {
        switch ($type) {
            case 'biscuit':
                return new BiscuitCake();

            case 'mousse':
                return new MousseCake();

            default:
                throw new \Exception("Неверное значение вида торта [{$type}]");
        }
    }
}