<?php


namespace ADFM\Controller;


use Slim\Http\Request;
use Slim\Http\Response;

class PageController extends Controller
{
    public function index(Request $request, Response $response, array $args)
    {
        return $this->render('index.html');
    }
}