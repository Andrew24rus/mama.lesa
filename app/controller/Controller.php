<?php

namespace ADFM\Controller;

use Slim\Views\PhpRenderer;

class Controller
{
    protected $ci;

    public function __construct($container)
    {
        $this->ci = $container;
    }

    public function render(string $template = null, array $params = [])
    {
        $response = $this->ci->get('response');

        $view = new PhpRenderer(__DIR__ . '/../themes');

        return $view->render($response, $template, $params);
    }
}
