<?php


namespace ADFM\Controller;


use ADFM\Controller\Traits\CakeByType;
use Slim\Http\Request;
use Slim\Http\Response;

class ParamsController extends Controller
{
    use CakeByType;

    public function getCakeParams(Request $request, Response $response, array $args)
    {
        $cakeType = $request->getParam('cakeType');

        if (!$cakeType) {
            throw new \Exception('Вид торта не задан');
        }

        $cake = $this->createCakeByType($cakeType);

        return $response->withJson($cake->getOptions());
    }
}