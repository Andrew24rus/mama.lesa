<?php


namespace ADFM\Controller;


use ADFM\Controller\Traits\CakeByType;
use ADFM\GoogleApi\Spreadsheet;
use ADFM\Helpers\Config;
use ADFM\Order\Order;
use ADFM\Payment\TinkoffPayment;
use Slim\Http\Request;
use Slim\Http\Response;

class OrderController extends Controller
{
    use CakeByType;

    /**
     * Создать заказ
     *
     * @Route("/api/order", methods={"POST"})
     */
    public function create(Request $request, Response $response, array $args)
    {
        $params = $request->getParam('order', []);

        list(
            'name' => $clientName,
            'phone' => $clientPhone,
            'date' => $deliveryDate,
            'type' => $cakeType,
            'weight' => $cakeWeight,
            'filling' => $cakeFilling,
            'color' => $cakeColor
        ) = $params;

        $cake = $this->createCakeByType($cakeType);
        $cake->setWeight($cakeWeight)
            ->setFilling($cakeFilling)
            ->setColor($cakeColor);

        $order = new Order($cake);
        $order->setClientName($clientName)
            ->setClientPhone($clientPhone)
            ->setDeliveryDate($deliveryDate)
            ->makeRecord();

        $paymentUrl = $this->initPayment($order->getOrderId(), 50000);

        return $response->withJson(compact('paymentUrl'));
    }

    /**
     * Подтверждение оплаты заказа
     *
     * @Route("/api/ordering-success/{orderId}", methods={"GET"})
     */
    public function completeOrder(Request $request, Response $response, array $args)
    {
        $orderId = $args['orderId'];
        $paymentId = $request->getParam('PaymentId', '');
        $amount = $request->getParam('Amount', 0) / 100;

        $spreadsheet = $this->openOrderSpreadsheet();
        $cell = $spreadsheet->findCellByValue($orderId, 'I', 'I');

        if ($cell) {
            $rowIndex = preg_replace('/\D/', '', $cell);

            $spreadsheet->write([$paymentId], "J{$rowIndex}");
            $spreadsheet->write([$amount], "K{$rowIndex}");
            $spreadsheet->write(['Оплачено'], "L{$rowIndex}");
        }

        return $response->withRedirect('/ordering-result');
    }

    /**
     * Инициирует оплату заказа
     *
     * @param string $orderId
     * @param int $amount
     *
     * @return string - url страницы оплаты
     */
    private function initPayment(string $orderId, int $amount): string
    {
        $payment = (new TinkoffPayment())->createMerchant();

        return $payment->setAmount($amount)
            ->setOrderId($orderId)
            ->setDescription('Предоплата заказа торта')
            ->setSuccessUrl($this->getUrl() . "/ordering-success/{$orderId}")
            ->init();
    }

    /**
     * Открывает таблицу с заказами
     *
     * @return Spreadsheet
     */
    private function openOrderSpreadsheet(): Spreadsheet
    {
        $spreadsheetId = Config::get('google')['spreadsheet']['id'];

        $spreadsheet = new Spreadsheet($spreadsheetId);
        $spreadsheet->setActiveSheet(2);

        return $spreadsheet;
    }

    private function getUrl(): string
    {
        $uri = $this->ci->get('request')->getUri();

        return $uri->getScheme() . '://' . $uri->getHost();
    }
}