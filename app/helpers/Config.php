<?php

namespace ADFM\Helpers;

use ADFM\Core\App;
use Illuminate\Support\Arr;

class Config
{
    public $config;

    private static $instance;

    private function __construct()
    {
        $config = [];

        $path = realpath(APP_PATH . 'config');
        $config_files = glob("{$path}/*.php");

        foreach ($config_files as $file) {
            $key = pathinfo($file, PATHINFO_FILENAME);

            $config[$key] = include $file;
        }

        $this->config = $config;
    }

    public static function getInstance()
    {
        if (empty(self::$instance)){
            self::$instance = new Config();
        }
        return self::$instance->config;
    }

    public static function set($key, $value) {
        self::$instance->config[$key] = $value;
    }

    public static function get($key, $default = null)
    {
        $config = self::getInstance();
        $param = Arr::get($config, $key);

        if (!$param) {
            $param = self::loadConfigFile($key);
        }

        return $param ?: $default;
    }

    private static function loadConfigFile(string $file_name)
    {
        $path = realpath(APP_PATH . "config/{$file_name}.php");

        if ($path) {
            $config = include $path;

            self::set($file_name, $config);

            return $config;
        }

        return null;
    }
}