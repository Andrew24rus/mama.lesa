<?php


namespace ADFM\GoogleApi;

use ADFM\GoogleApi\Exceptions\CredentialsFileNotFoundException;
use ADFM\GoogleApi\Exceptions\SpreadsheetSheetNotFoundException;
use Google_Client;
use Google_Service_Sheets;

class Spreadsheet
{
    /**
     * @var string - Идентификатор документа
     */
    private $spreadsheetId;

    /**
     * @var string
     */
    private $credentialsFileName = 'credentials.json';

    /**
     * @var int - Индекс активной страницы
     */
    private $sheetIndex = 0;

    /**
     * @var Google_Service_Sheets
     */
    private $service;

    public function __construct(string $spreadsheetId)
    {
        $this->spreadsheetId = $spreadsheetId;

        $this->putApplicationCredentials();

        $this->service = $this->initService();
    }

    private function putApplicationCredentials()
    {
        $googleAccountKeyFilePath = getenv('DOCUMENT_ROOT') . '/../' . $this->credentialsFileName;
        $googleAccountKeyFilePath = realpath($googleAccountKeyFilePath);

        if (!$googleAccountKeyFilePath) {
            throw new CredentialsFileNotFoundException(
                $this->credentialsFileName . ' not found'
            );
        }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath);
    }

    private function initService(): Google_Service_Sheets
    {
        $client = $this->initClient();

        return new Google_Service_Sheets($client);
    }

    private function initClient(): Google_Client
    {
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope('https://www.googleapis.com/auth/spreadsheets');

        return $client;
    }

    private function getActiveSheetTitle()
    {
        $sheet = $this->getActiveSheet();
        $sheetTitle = $sheet->getProperties()->getTitle();

        return $sheetTitle;
    }

    public function getSpreadsheet(): \Google_Service_Sheets_Spreadsheet
    {
        return $this->service->spreadsheets->get($this->spreadsheetId);
    }

    /**
     * @return \Google_Service_Sheets_Sheet
     *
     * @throws SpreadsheetSheetNotFoundException
     */
    public function getActiveSheet(): \Google_Service_Sheets_Sheet
    {
        $spreadsheet = $this->getSpreadsheet();
        $sheets = $spreadsheet->getSheets();

        if (!isset($sheets[$this->sheetIndex])) {
            throw new SpreadsheetSheetNotFoundException(
                sprintf('Sheet with id [%d] not found', $this->sheetIndex)
            );
        }

        return $sheets[$this->sheetIndex];
    }

    /**
     * @param int $sheetIndex
     */
    public function setActiveSheet(int $sheetIndex)
    {
        $this->sheetIndex = $sheetIndex;
    }

    /**
     * @param string $range
     *
     * @return array|null
     *
     */
    public function read(string $range)
    {
        $sheetTitle = $this->getActiveSheetTitle();

        $range = "{$sheetTitle}!{$range}";

        $data = $this->service->spreadsheets_values->get($this->spreadsheetId, $range);

        return $data->getValues();
    }

    /**
     * @param array $data
     * @param string $startCell
     */
    public function write(array $data, string $startCell)
    {
        $valueRange = new \Google_Service_Sheets_ValueRange([
            'values' => [$data]
        ]);

        $range = "{$this->getActiveSheetTitle()}!{$startCell}";

        $options = ['valueInputOption' => 'USER_ENTERED'];

        $this->service
            ->spreadsheets_values
            ->update($this->spreadsheetId, $range, $valueRange, $options);
    }

    /**
     * @param array $data
     */
    public function insert(array $data)
    {
        $rowsCount = count($this->read('A1:A'));

        $startRow = $rowsCount + 1;
        $startCell = "A{$startRow}";

        $this->write($data, $startCell);
    }

    /**
     * Поиск координаты ячейки по её значению
     *
     * @param string $value
     * @param string $colStart
     * @param string $colEnd
     * @return string|null
     */
    public function findCellByValue(string $value, string $colStart = 'A', string $colEnd = 'Z')
    {
        $rows = $this->read("{$colStart}1:{$colEnd}");

        foreach ($rows as $rowIndex => $row) {
            $colIndex = array_search($value, $row);

            if ($colIndex !== false) {
                $colIndex = chr(ord($colStart)+$colIndex);
                $rowIndex++;

                return "{$colIndex}{$rowIndex}";
            }
        }

        return null;
    }
}