<?php


namespace ADFM\GoogleApi\Exceptions;


class CredentialsFileNotFoundException extends \Exception
{
    protected $message = 'Credentials file not found';
}