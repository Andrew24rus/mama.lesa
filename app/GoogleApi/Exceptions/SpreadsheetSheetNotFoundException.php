<?php


namespace ADFM\GoogleApi\Exceptions;


class SpreadsheetSheetNotFoundException extends \Exception
{
    protected $message = 'Sheet not found';
}