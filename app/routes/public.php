<?php

use ADFM\Controller\OrderController;
use ADFM\Controller\PageController;
use ADFM\Core\App;

App::get('/ordering-success/{orderId}', OrderController::class.':completeOrder');
App::get('[/{path:.*}]', PageController::class.':index');