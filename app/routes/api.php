<?php

use ADFM\Controller\OrderController;
use ADFM\Controller\ParamsController;
use ADFM\Core\App;
use ADFM\Middlewares\JsonResponse;

App::group('/api', function ($app) {
    $app->post('/order', OrderController::class.':create');
    $app->get('/params', ParamsController::class.':getCakeParams');
    $app->get('/cakes-in-stock', ParamsController::class.':getCakesInStock');
})->add(new JsonResponse());