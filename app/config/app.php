<?php

return [
    'providers' => [
        \ADFM\Providers\ExceptionProvider::class,
        \ADFM\Providers\RouteProvider::class,
    ]
];
