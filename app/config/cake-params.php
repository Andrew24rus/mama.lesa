<?php

use Carbon\Carbon;

$tomorrow = Carbon::tomorrow();

function getBiscuitMinDate()
{
    $minDate = Carbon::tomorrow() // After 3 days
        ->startOfDay()
        ->addDays(3)
        ->format('Y-m-d');

    return $minDate;
}

function getMousseMinDate()
{
    $minDate = Carbon::now() // Next week Tuesday
        ->startOfDay()
        ->addWeek()
        ->addDays(2)
        ->format('Y-m-d');

    return $minDate;
}

return [
    'weight' => [
        'min' => 1, 'max' => 5
    ],

    'types' => [
        ['value' => 'Бисквит', 'price' => 1400, 'minOrderingDate' => getBiscuitMinDate()],
        ['value' => 'Мусс', 'price' => 1400, 'minOrderingDate' => getMousseMinDate()]
    ],

    'fillings' => [
        ['value' => 'Черника', 'image' => 'https://e3.edimdoma.ru/data/recipes/0005/6232/56232-ed4_wide.jpg?1468434722'],
        ['value' => 'Брусника', 'image' => 'https://ist1.objorka.com/img0002/61/261_017739r_2816_6hi.jpg'],
        ['value' => 'Малина', 'image' => 'https://i.pinimg.com/736x/8d/0b/1d/8d0b1daf7970a922a22696db0a641d25.jpg'],
        ['value' => 'Шоколад', 'image' => 'https://www.maggi.ru/data/images/recept/img640x500/recept_3245_smld.jpg']
    ],

    'colors' => [
        ['value' => 'Желтый', 'hex' => '#FFD22E'],
        ['value' => 'Голубой', 'hex' => '#5BC3D1'],
        ['value' => 'Зеленый', 'hex' => '#5BD175'],
        ['value' => 'Красный', 'hex' => '#FF2D02']
    ]
];