<?php

namespace ADFM\exceptions;

use Slim\Handlers\PhpError;
use Slim\Http\Request;
use Slim\Http\Response;

class ErrorExceptions
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, $exception)
    {
        $error = new PhpError(true);

        return $error($request, $response, $exception);
    }
}
