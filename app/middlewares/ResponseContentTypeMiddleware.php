<?php


namespace ADFM\Middlewares;


use ADFM\Middlewares\Exceptions\InvalidJsonResponseException;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Slim\Http\Request;
use Slim\Http\Response;

class ResponseContentTypeMiddleware
{
    const STATUS_SUCCESS      = ['code' => 200, 'status' => 'OK'];
    const STATUS_CLIENT_ERROR = ['code' => 400, 'status' => 'Client Error'];
    const STATUS_SERVER_ERROR = ['code' => 500, 'status' => 'Server Error'];

    /**
     * Выполнение запроса и обработка исключений
     *
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return array
     */
    protected function doRequest(Request $request, Response $response, $next): array
    {
        try {
            $response = $next($request, $response);

            $data = $this->parseResponseData($response);

        } catch (ValidationException $e) {
            return $this->getValidationErrorResponse($e);
        } catch (\Exception $e) {
            return $this->getServerErrorResponse($e);
        }

        return $this->getSuccessResponse($data);
    }

    /**
     * Парсит тело ответа из json в массив
     *
     * @param Response $response
     *
     * @return array
     *
     * @throws InvalidJsonResponseException - Если тело ответа не в формате Json
     */
    private function parseResponseData(Response $response): array
    {
        $jsonData = $response->getBody()->__toString();
        $arrayData = json_decode($jsonData, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidJsonResponseException(json_last_error_msg());
        }

        return $arrayData;
    }

    private function getSuccessResponse(array $data)
    {
        return array_merge(self::STATUS_SUCCESS, [
            'data' => $data
        ]);
    }

    private function getValidationErrorResponse(ValidationException $e)
    {
        $messages = $e->validator->getMessageBag()->messages();
        $messages = Arr::flatten(array_values($messages));

        $error = array_merge($this->makeErrorData($e), [
            'messages' => $messages
        ]);

        return array_merge(self::STATUS_CLIENT_ERROR, [
            'error' => $error
        ]);
    }

    private function getClientErrorResponse(Exception $e)
    {
        return array_merge(self::STATUS_CLIENT_ERROR, [
            'error' => $this->makeErrorData($e)
        ]);
    }

    private function getServerErrorResponse(Exception $e)
    {
        return array_merge(self::STATUS_SERVER_ERROR, [
            'error' => $this->makeErrorData($e)
        ]);
    }

    private function makeErrorData(Exception $e): array
    {
        $error = [
            'messages' => [$e->getMessage()],
            'code' => $e->getCode(),
            'exception' => (new \ReflectionClass($e))->getShortName()
        ];

        return $error;
    }
}