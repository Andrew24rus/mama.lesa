<?php


namespace ADFM\Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;

class JsonResponse extends ResponseContentTypeMiddleware
{
    public function __invoke(Request $request, Response $response, $next)
    {
        $data = $this->doRequest($request, $response, $next);

        return $response
            ->withHeader('Content-type', 'application/json')
            ->withStatus($data['code'])
            ->withJson($data);
    }
}