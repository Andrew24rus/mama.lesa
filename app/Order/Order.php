<?php


namespace ADFM\Order;


use ADFM\GoogleApi\Spreadsheet;
use ADFM\Helpers\Config;
use ADFM\Order\Exceptions\DeliveryDateToEarlyException;
use ADFM\Order\Product\IProduct;
use Carbon\Carbon;

class Order
{
    /**
     * @var IProduct
     */
    private $product;

    /**
     * @var Spreadsheet
     */
    private $spreadsheet;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $clientName;

    /**
     * @var string
     */
    private $clientPhone;

    /**
     * @var Carbon
     */
    private $deliveryDate;

    public function __construct(IProduct $product)
    {
        $this->product = $product;
        $this->orderId = $this->generateOrderId();
        $this->spreadsheet = $this->openSpreadsheet(2);
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function setClientName(string $clientName): Order
    {
        $this->clientName = $clientName;

        return $this;
    }

    public function setClientPhone(string $clientPhone): Order
    {
        $clientPhone = preg_replace('/\D/', '', $clientPhone);

        $this->clientPhone = $clientPhone;

        return $this;
    }

    public function setDeliveryDate(string $date): Order
    {
        $date = Carbon::createFromFormat('Y-m-d', $date);
        $minDate = $this->product->getMinOrderingDate();

        if ($date < $minDate) {
            $msg = "Дата доставки должна быть не позднее чем {$minDate->format('d.m.Y')}";

            throw new DeliveryDateToEarlyException($msg);
        }

        $this->deliveryDate = $date;

        return $this;
    }

    public function makeRecord()
    {
        $this->spreadsheet->insert(array_values($this->__toArray()));
    }

    private function generateOrderId(): string
    {
        return md5(uniqid());
    }

    private function openSpreadsheet($sheetIndex = 0): Spreadsheet
    {
        $spreadsheetId = Config::get('google')['spreadsheet']['id'];

        $spreadsheet = new Spreadsheet($spreadsheetId);
        $spreadsheet->setActiveSheet($sheetIndex);

        return $spreadsheet;
    }

    public function __toArray(): array
    {
        return array_merge(
            [
                'clientName' => $this->clientName,
                'clientPhone' => $this->clientPhone,
                'deliveryDate' => $this->deliveryDate->format('d.m.Y')
            ],

            $this->product->__toArray(),

            [
                'amount' => $this->product->getAmount(),
                'orderId' => $this->orderId,
                'paymentId' => '',
                'payedSum' => '',
                'status' => 'Не оплачено',
                'created_at' => Carbon::now()->format('d.m.Y')
            ]
        );
    }
}