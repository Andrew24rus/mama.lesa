<?php


namespace ADFM\Order\Exceptions;


class OrderException extends \Exception
{
    protected $code = 400;
}