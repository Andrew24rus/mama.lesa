<?php


namespace ADFM\Order\Product\Cake\Exceptions;


class CakeException extends \Exception
{
    protected $code = 400;
}