<?php


namespace ADFM\Order\Product\Cake;


use ADFM\Order\Product\Cake\Exceptions\ColorNotAvailableException;
use ADFM\Order\Product\Cake\Exceptions\WeightNotDefinedException;
use ADFM\Order\Product\Cake\Exceptions\WeightOutOfRangeException;
use ADFM\Order\Product\IProduct;

abstract class Cake implements IProduct
{
    protected $options = [
        'fillings' => [
            'blueberry' => [
                'label' => 'Черника',
                'image' => 'https://e3.edimdoma.ru/data/recipes/0005/6232/56232-ed4_wide.jpg?1468434722'
            ],
            'lingonberry' => [
                'label' => 'Брусника',
                'image' => 'https://ist1.objorka.com/img0002/61/261_017739r_2816_6hi.jpg'
            ],
            'raspberry' => [
                'label' => 'Малина',
                'image' => 'https://i.pinimg.com/736x/8d/0b/1d/8d0b1daf7970a922a22696db0a641d25.jpg'
            ],
            'chocolate' => [
                'label' => 'Шоколад',
                'image' => 'https://www.maggi.ru/data/images/recept/img640x500/recept_3245_smld.jpg'
            ]
        ],
        'colors' => [
            'yellow' => ['label' => 'Желтый', 'hex' => '#FFD22E'],
            'cyan'   => ['label' => 'Голубой', 'hex' => '#5BC3D1'],
            'green'  => ['label' => 'Зеленый', 'hex' => '#5BD175'],
            'red'    => ['label' => 'Красный', 'hex' => '#FF2D02']
        ]
    ];

    /**
     * Цвет
     *
     * @var array
     */
    protected $color;

    /**
     * Начинка
     *
     * @var array
     */
    protected $filling;

    /**
     * Вес
     *
     * @var float
     */
    protected $weight;

    abstract public function getWeightMin(): float;
    abstract public function getWeightMax(): float;
    abstract public function getWeightStep(): float;
    abstract public function getWeightRange(): array;

    public function getWeight(): float
    {
        if (is_null($this->weight)) {
            throw new WeightNotDefinedException('Вес торта не задан');
        }

        return $this->weight;
    }

    public function setColor(string $color): Cake
    {
        $colors = $this->options['colors'];

        if (!isset($colors[$color])) {
            throw new ColorNotAvailableException("Недопустимое значение цвета [{$color}]");
        }

        $this->color = $colors[$color];

        return $this;
    }

    public function setFilling(string $filling): Cake
    {
        $fillings = $this->options['fillings'];

        if (!isset($fillings[$filling])) {
            throw new ColorNotAvailableException("Недопустимое значение начинки [{$filling}]");
        }

        $this->filling = $fillings[$filling];

        return $this;
    }

    public function setWeight(float $weight): Cake
    {
        $minWeight = $this->getWeightMin();
        $maxWeight = $this->getWeightMax();

        if ($weight > $maxWeight || $weight < $minWeight) {
            $msg = "Вес торта должен быть в диапазоне от {$minWeight} до ${maxWeight} кг. Текущее значение [{$weight}]";

            throw new WeightOutOfRangeException($msg);
        }

        $this->weight = $weight;

        return $this;
    }

    /**
     * Получить стоимость торта
     *
     * @return float|int|null
     */
    public function getAmount()
    {
        return $this->getPrice() * $this->getWeight();
    }

    /**
     * Получить опции, доступные при создании торта
     *
     * @return array
     */
    public function getOptions(): array
    {
        return array_merge($this->options, [
            'price' => $this->getPrice(),
            'weight' => $this->getWeightRange()
        ]);
    }

    public function __toArray(): array
    {
        return [
            'type' => $this->getTitle(),
            'weight' => $this->weight,
            'filling' => $this->filling['label'],
            'color' => $this->color['label']
        ];
    }
}