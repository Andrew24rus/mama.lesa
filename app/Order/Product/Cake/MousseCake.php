<?php


namespace ADFM\Order\Product\Cake;


use Carbon\Carbon;

class MousseCake extends Cake
{
    private $title = 'Мусс';
    private $price = 1400;
    private $weightRange = [
        'min' => 1,
        'max' => 10,
        'step' => 0.5
    ];

    public function getWeightMin(): float
    {
        return $this->weightRange['min'];
    }

    public function getWeightMax(): float
    {
        return $this->weightRange['max'];
    }

    public function getWeightStep(): float
    {
        return $this->weightRange['step'];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMinOrderingDate(): Carbon
    {
        // Next week Tuesday
        return Carbon::now()->startOfDay()->addWeek()->addDays(2);
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getWeightRange(): array
    {
        return $this->weightRange;
    }
}