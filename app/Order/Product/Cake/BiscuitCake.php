<?php


namespace ADFM\Order\Product\Cake;


use Carbon\Carbon;

class BiscuitCake extends Cake
{
    private $title = 'Бисквит';
    private $price = 1400;
    private $weightRange = [
        'min' => 2.8,
        'max' => 9.6,
        'step' => 1.7
    ];

    public function getWeightMin(): float
    {
        return $this->weightRange['min'];
    }

    public function getWeightMax(): float
    {
        return $this->weightRange['max'];
    }

    public function getWeightStep(): float
    {
        return $this->weightRange['step'];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMinOrderingDate(): Carbon
    {
        return Carbon::tomorrow()->startOfDay()->addDays(3);
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getWeightRange(): array
    {
        return $this->weightRange;
    }
}