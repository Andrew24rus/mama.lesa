<?php


namespace ADFM\Order\Product;


use Carbon\Carbon;

interface IProduct
{
    public function getAmount();

    public function getPrice(): int;

    public function getTitle(): string;

    public function getMinOrderingDate(): Carbon;

    public function __toArray(): array;
}