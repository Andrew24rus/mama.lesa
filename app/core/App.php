<?php

namespace ADFM\Core;


use ADFM\Helpers\Config;
use Dotenv\Dotenv;
use Slim\Container;
use Tracy\Debugger;

define('BASE_PATH', __DIR__ . '/../../');
define('APP_PATH', BASE_PATH . '/app/');
define('CORE_PATH', APP_PATH . '/core/');
define('CONTENT_PATH', BASE_PATH . '/content/');
define('CONFIG_PATH', BASE_PATH . '/config/');
define('ROUTES_PATH', APP_PATH . '/routes/');
define('PLUGINS_PATH', APP_PATH . '/modules/');

error_reporting(E_ERROR | E_PARSE);

/**
 * @method static getContainer()
 */
class App
{
    private static $app;

    public function __construct()
    {
        require_once __DIR__  . '/../../vendor/autoload.php';

        $dotenv = Dotenv::createImmutable(realpath(__DIR__ . '/../..'));
        $dotenv->load();

        date_default_timezone_set($_ENV['TIMEZONE']);

        //режим разработчика
        if (Config::get('main')['debug']) {
            Debugger::enable();
        }

        $container = new Container([
            'settings' => [
                'displayErrorDetails' => true,
            ],
        ]);

        self::$app = new \Slim\App($container);

        $this->bootProviders(
            $this->registerProviders($container)
        );

        self::$app->run();
    }

    /**
     * @param $container
     *
     * @return ProviderInitiator
     */
    private function registerProviders($container)
    {
        $providers = Config::get('app')['providers'];

        $initiator = new ProviderInitiator($container, $providers);
        $initiator->register();

        return $initiator;
    }

    /**
     * @param ProviderInitiator $initiator
     */
    private function bootProviders(ProviderInitiator $initiator)
    {
        $initiator->boot();
    }

    public static function __callStatic(string $name, $arguments)
    {
        return self::$app->$name(...$arguments);
    }
}
