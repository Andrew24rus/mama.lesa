<?php


namespace ADFM\Core\Validation\Traits;


use ADFM\Core\Validation\Validator;

trait ValidatesRequests
{
    public function validate(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }
}