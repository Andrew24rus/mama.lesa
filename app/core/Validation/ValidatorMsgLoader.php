<?php


namespace ADFM\Core\Validation;


use Illuminate\Support\Arr;

class ValidatorMsgLoader
{
    private $config_file = 'validation.php';

    private $config;

    public function __construct()
    {
        $this->config = $this->readConfigFile();
    }

    private function readConfigFile() : array
    {
        $config = include realpath(__DIR__ . '/../../config/' . $this->config_file);

        return $config ?: [];
    }

    private function getByKey($key, $default = [])
    {
        return Arr::get($this->config, $key, $default);
    }

    public function getMessages() : array
    {
        $messages = $this->getByKey('messages');
        $custom = $this->getByKey('custom');

        foreach ($custom as $field => $rules) {
            foreach ($rules as $rule => $text) {
                $messages["${field}.${rule}"] = $text;
            }
        }

        return $messages;
    }

    public function getAttributes() : array
    {
        return $this->getByKey('attributes');
    }


}
