<?php


namespace ADFM\Core\Validation;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;

/**
 * @method static make(mixed[] $order, array $array)
 */
class Validator
{
    /**
     * @var Factory
     */
    private static $factory;

    /**
     * @var ValidatorMsgLoader
     */
    private static $msg_loader;

    private static function getInstance()
    {
        if (!self::$factory instanceof Factory) {
            $filesystem = new Filesystem();
            $fileloader = new FileLoader($filesystem, '');
            $translator = new Translator($fileloader, 'en_US');

       //     $db_manager = Config::get('container')->db_manager;

            self::$factory = new Factory($translator);
       //     self::$factory->setPresenceVerifier(new DatabasePresenceVerifier($db_manager));

            self::$msg_loader = new ValidatorMsgLoader();
        }

        return self::$factory;
    }

    public static function __callStatic($method, $args)
    {
        $instance = self::getInstance();

        if ($method === 'make') {
            $args[] = self::$msg_loader->getMessages();
            $args[] = self::$msg_loader->getAttributes();
        }

        $validator = $instance->$method(...$args);
        $validator->validate();
    }
}
