<?php


namespace ADFM\Core\Validation\Rules;


use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Contracts\Validation\Rule;

class MatchOldPassword implements Rule
{
    private $user;

    public function __construct(EloquentUser $user = null)
    {
        $this->user = $user ?: Sentinel::check();
    }

    public function passes($attribute, $value)
    {
        if ($this->user) {
            return Sentinel::validateCredentials($this->user, [
                'password' => $value
            ]);
        }

        return false;
    }

    public function message()
    {
        return 'Старый пароль введен не верно';
    }
}
