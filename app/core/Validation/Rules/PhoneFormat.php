<?php


namespace ADFM\Core\Validation\Rules;


use Illuminate\Contracts\Validation\Rule;

class PhoneFormat implements Rule
{
    public function passes($attribute, $value)
    {
        $phone = preg_replace('/\D/', '', $value);

        return strlen($phone) === 11;
    }

    public function message()
    {
        return 'Не верный формат номера телефона';
    }
}
