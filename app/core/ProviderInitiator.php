<?php


namespace ADFM\Core;


use ADFM\Providers\ServiceProvider;
use Slim\Container;

class ProviderInitiator
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var ServiceProvider[]
     */
    private $providers;

    public function __construct(Container $container, array $provider_list)
    {
        $this->container = $container;

        foreach ($provider_list as $provider) {
            $this->setProvider(new $provider($container));
        }
    }

    /**
     * @param ServiceProvider $provider
     */
    private function setProvider(ServiceProvider $provider)
    {
        $this->providers[] = $provider;
    }

    public function register()
    {
        foreach ($this->providers as $provider) {
            $provider->register();
        }
    }

    public function boot()
    {
        foreach ($this->providers as $provider) {
            $provider->boot();
        }
    }
}