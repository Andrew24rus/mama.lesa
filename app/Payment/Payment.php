<?php


namespace ADFM\Payment;


use ADFM\Payment\Merchant\MerchantApiInterface;

abstract class Payment
{
    abstract public function createMerchant(): MerchantApiInterface;
}