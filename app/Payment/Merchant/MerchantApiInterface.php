<?php


namespace ADFM\Payment\Merchant;


interface MerchantApiInterface
{
    /**
     * Инициализация оплаты
     *
     * @return string - Ссылка на страницу оплаты
     */
    public function init(): string;

    /**
     * Задать сумму к оплате в копейках
     *
     * @param int $amount
     *
     * @return MerchantApiInterface
     */
    public function setAmount(int $amount): MerchantApiInterface;

    /**
     * Задать описание платежа
     *
     * @param string $description
     *
     * @return MerchantApiInterface
     */
    public function setDescription(string $description): MerchantApiInterface;

    /**
     * Задать идентификатор заказа в системе продавца
     *
     * @param string $orderId
     *
     * @return MerchantApiInterface
     */
    public function setOrderId(string $orderId): MerchantApiInterface;

    /**
     * Задать страницу успеха
     *
     * @param string $url
     *
     * @return MerchantApiInterface
     */
    public function setSuccessUrl(string $url): MerchantApiInterface;

    /**
     * Задать страницу ошибки
     *
     * @param string $url
     *
     * @return MerchantApiInterface
     */
    public function setFailUrl(string $url): MerchantApiInterface;
}