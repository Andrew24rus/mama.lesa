<?php


namespace ADFM\Payment;


use ADFM\Payment\Merchant\MerchantApiInterface;
use ADFM\Payment\Merchant\TinkoffMerchantApi;

class TinkoffPayment extends Payment
{
    public function createMerchant(): MerchantApiInterface
    {
        $terminalKey = $_ENV['TINKOFF_TERMINAL_KEY'];
        $secretKey = $_ENV['TINKOFF_SECRET_KEY'];

        return new TinkoffMerchantApi($terminalKey, $secretKey);
    }
}