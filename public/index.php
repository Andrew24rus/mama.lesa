<?php

/*стартуем сессию*/

use ADFM\Core\App;

session_start();

/*Отладка*/
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/*Старт приложения*/
require_once '../app/core/App.php';

new App();
