import Http from "./Http"

export const fetchCakeParams = () => {
    return Http.get('/api/params')
}

// export const fetchStock = () => {
//     return Http.get('/api/cakes-in-stock')
// }

export const createOrder = (order) => {
    return Http.post('/api/order', { order })
}