import store from "../redux/store"
import {addAlert} from "../redux/actions/alertActions"

// For debug only
//const domain = 'http://mama-lesa.localhost'

class Http {
    static get(url, params = {}) {
        return Http._do(url + new URLSearchParams(params));
    }

    static post(url, params = {}) {
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(params)
        }

        return Http._do(url, options)
    }

    static _do = (url, options = {}) => {
        return fetch(/*domain +*/ url, options)
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    Http._handleError(response.error)
                } else {
                    return response.data
                }
            })
            .catch(error => console.error(error))
    }

    static _handleError(error) {
        error.messages?.map(message => store.dispatch(addAlert(message)))
    }
}

export default Http