import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import OrderReducer from "./reducers/order"
import CakeParamsReducer from "./reducers/params"
import AlertReducer from "./reducers/alert"

const initialState = {}
const reducers = combineReducers({
    alerts: AlertReducer,
    order: OrderReducer,
    cakeParams: CakeParamsReducer
})

const store = createStore(
    reducers,
    initialState,
    compose(
        applyMiddleware(
            thunk
        )
    )
)

export default store