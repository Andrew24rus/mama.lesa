import {ALERT_ADD, ALERT_CLEAR, ALERT_DELETE} from "../types"

const AlertReducer = function(state = [], action) {
    const { payload } = action

    switch (action.type) {
        case ALERT_ADD:
            if ((state.indexOf(payload)) === -1) {
                state.push(payload)
            }

            return state

        case ALERT_DELETE:
            state = state.filter((alert, i) => i !== payload)

            return state

        case ALERT_CLEAR:
            return []

        default:
            return state
    }
}

export default AlertReducer