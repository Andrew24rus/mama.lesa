const StockReducer = function(state={}, action) {
    const { payload } = action

    switch (action.type) {
        case 'STOCK_DATA_SET':
            return payload

        case 'STOCK_FETCHING':
            return {
                ...state,
                isFetching: !!payload
            }

        default:
            return state;
    }
}

export default StockReducer