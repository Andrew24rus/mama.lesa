import {ORDER_DATA_CLEAR, ORDER_DATA_SET, ORDER_SENDING} from "../types";

const fillable = ['name', 'phone', 'date', 'type', 'weight', 'filling', 'color']

const statePropExist = (prop) => fillable.find(key => prop === key)

const OrderReducer = function(state = {}, action) {
    const { payload } = action

    switch (action.type) {
        case ORDER_DATA_SET:
            const propName =  Object.keys(payload)[0]

            if (statePropExist(propName)) {
                return {...state, ...payload}
            }

            return state

        case ORDER_SENDING:
            return {
                ...state,
                isSending: !!payload
            }

        case ORDER_DATA_CLEAR:
            return {}

        default:
            return state
    }
}

export default OrderReducer