import {PARAMS_FETCHING, PARAMS_SET} from "../types";

const ParamsReducer = function(state= {}, action) {
    const { payload } = action

    switch (action.type) {
        case PARAMS_SET:
            return payload

        case PARAMS_FETCHING:
            return {
                ...state,
                isFetching: !!payload
            }

        default:
            return state;
    }
}

export default ParamsReducer