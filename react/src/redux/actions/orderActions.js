import {createOrder} from "../../network/requests"
import {ORDER_DATA_CLEAR, ORDER_DATA_SET, ORDER_SENDING} from "../types";


export const setOrderData = (key, value) => {
    return {
        type: ORDER_DATA_SET,
        payload: {[key]: value}
    }
}

export const clearOrderData = () => {
    return {
        type: ORDER_DATA_CLEAR
    }
}

export const setSendingOrder = (sending = true) => {
    return {
        type: ORDER_SENDING,
        payload: sending
    }
}

export const sendOrder = order => dispatch => {
    dispatch(setSendingOrder())

    return createOrder(order)
        .finally(() => dispatch(setSendingOrder(false)))
}