import {ALERT_ADD, ALERT_CLEAR, ALERT_DELETE} from "../types"

export const addAlert = alert => {
    return {
        type: ALERT_ADD,
        payload: alert
    }
}

export const deleteAlert = index => {
    return {
        type: ALERT_DELETE,
        payload: index
    }
}

export const clearAlerts = () => {
    return {
        type: ALERT_CLEAR
    }
}