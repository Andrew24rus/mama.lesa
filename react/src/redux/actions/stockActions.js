import { fetchStock } from '../../network/requests'

export const fetchParams = () => dispatch => {
    dispatch(setFetchingStock())

    fetchStock()
        .then(params => {
            dispatch({
                type: 'STOCK_DATA_SET',
                payload: params.data
            })
        }).then(() => dispatch(setFetchingStock(false)))
}

export const setFetchingStock = (fetching = true) => {
    return {
        type: 'STOCK_FETCHING',
        payload: fetching
    }
}