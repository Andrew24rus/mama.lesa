import { fetchCakeParams } from '../../network/requests'
import {PARAMS_FETCHING, PARAMS_SET} from "../types";

export const setParams = data => {
    return {
        type: PARAMS_SET,
        payload: data
    }
}

export const setFetchingParams = (fetching = true) => {
    return {
        type: PARAMS_FETCHING,
        payload: fetching
    }
}

export const fetchParams = () => dispatch => {
    dispatch(setFetchingParams())

    return fetchCakeParams()
        .then(params => dispatch(setParams(params)))
        .finally(() => dispatch(setFetchingParams(false)))
}