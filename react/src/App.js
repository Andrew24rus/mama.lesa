import React from "react"
import {BrowserRouter} from 'react-router-dom'
import Navigator from "./navigation/Navigator"
import {Provider} from "react-redux"
import store from "./redux/store"

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div className="container">
                        <Navigator initialRoute={'ordering'} />
                    </div>
                </BrowserRouter>
            </Provider>
        )
    }
}

export default App