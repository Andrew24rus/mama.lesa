import React from "react";
import * as ReactDOM from "react-dom";
import App from "../App";

it('Should render App without crashes', () => {
    const div = document.createElement('div')

    ReactDOM.render(<App />, div)
    ReactDOM.unmountComponentAtNode(div)
})