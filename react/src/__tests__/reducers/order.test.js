import OrderReducer from "../../redux/reducers/order";
import {clearOrderData, setOrderData} from "../../redux/actions/orderActions";


it('Set order data by key', () => {
    const key = 'name';
    const value = 'Andrew';

    let newState = OrderReducer({}, setOrderData(key, value))

    expect(newState[key]).toEqual(value)
})

it('Set order data by invalid key must be "undefined"', () => {
    const key = 'invalidKey';
    const value = 'invalidData';

    let newState = OrderReducer({}, setOrderData(key, value))

    expect(newState[key]).toEqual(undefined)
})

it('Must clear order state', () => {
    let newState = OrderReducer({}, clearOrderData())

    expect(newState).toEqual({})
})