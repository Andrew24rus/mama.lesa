import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {fetchParams, setFetchingParams, setParams} from "../../redux/actions/paramsActions";
import * as Requests from "../../network/requests";

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Params:', () => {
    let store
    let expectation
    let dispatched

    beforeEach(() => {
        expectation = {
            data: {
                types: [],
                filling: []
            }
        }

        const spy = jest.spyOn(Requests, 'fetchCakeParams')
        spy.mockReturnValue(Promise.resolve(expectation))

        store = mockStore({})
        store.clearActions()

        dispatched = store.dispatch(fetchParams())
    })

    afterEach(() => {
        jest.clearAllMocks()
    })

    const findAction = (type, fromLast = false) => {
        let actions = store.getActions()

        if (fromLast) {
            actions = actions.reverse()
        }

        return actions.find(a => a.type === type)
    }

    it('Should set fetched data', () => {
        return dispatched
            .then(() => {
                const exp = setParams(expectation.data)
                const action = findAction(exp.type)

                expect(action).toEqual(exp)
            })
    })

    it('Should mark isFetching to true before fetching', () => {
        return dispatched.then(() => {
            const exp = setFetchingParams(true)
            const action = findAction(exp.type)

            expect(action).toEqual(exp)
        })
    })

    it('Should mark isFetching to false after fetching', () => {
        return dispatched.then(() => {
            const exp = setFetchingParams(false)
            const action = findAction(exp.type, true)

            expect(action).toEqual(exp)
        })
    })
})