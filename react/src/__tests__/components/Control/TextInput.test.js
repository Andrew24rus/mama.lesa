import React from "react";
import { shallow } from "enzyme";
import TextInput from "../../../components/Form/Control/TextInput"
import {testEvents} from "./_Input.helper";

const setUp = props => shallow(<TextInput {...props} />)

describe('<TextInput />', () => {
    let component
    let input

    beforeEach(() => {
        component = setUp()
        input = component.find('input')
    })

    it('Should render component', () => {
        expect(input).toHaveLength(1)
    })

    testEvents(TextInput, 'input')
})