import React from "react";
import { shallow } from "enzyme";

export const testEvents = (Component, targetSelector) => {
    const setUp = (props) => shallow(<Component {...props} />)

    describe('Simulate events', () => {
        let component
        let target
        let event

        beforeEach(() => {
            component = setUp()
            target = component.find(targetSelector)
            event = { target: { value: 'lorem ipsum' } }
        })

        afterEach(() => {
            jest.clearAllMocks()
        })

        it('Should update state value on input event', () => {
            target.simulate('input', event)

            expect(component.state('value')).toBe(event.target.value)
        })

        it('Should update state value on change event', () => {
            target.simulate('change', event)

            expect(component.state('value')).toBe(event.target.value)
        })

        it('Should call onInput prop func with arg value when input event triggered', () => {
            const onInput = jest.fn()
            const component = setUp({ onInput })

            component.find(targetSelector).simulate('input', event)

            expect(onInput).toBeCalledTimes(1)
            expect(onInput).toBeCalledWith(event.target.value)
        })

        it('Should call onChange prop func with arg value when change event triggered', () => {
            const onChange = jest.fn()
            const component = setUp({ onChange })

            component.find(targetSelector).simulate('change', event)

            expect(onChange).toBeCalledTimes(1)
            expect(onChange).toBeCalledWith(event.target.value)
        })
    })
}

it('☺', () => {
    expect(1).toBe(1)
})