import React from "react";
import { shallow } from "enzyme";
import TextArea from "../../../components/Form/Control/TextArea";
import { testEvents } from "./_Input.helper";

const setUp = props => shallow(<TextArea {...props} />)

describe('<TextArea />:', () => {
    let component
    let textarea

    beforeEach(() => {
        component = setUp()
        textarea = component.find('textarea')
    })

    it('Should render component', () => {
        expect(textarea).toHaveLength(1)
    })

    it('Should render component with initial value', () => {
        const props = { value: 'lorem ipsum' }
        const textarea = setUp(props).find('textarea')

        expect(textarea.text()).toBe(props.value)
    })

    testEvents(TextArea, 'textarea')
})
