import React from "react";
import StageContainer from "../../components/StageContainer";
import { shallow } from 'enzyme'

const setUp = (props, children) => shallow(<StageContainer {...props} children={children} />)

const div = (key) => <div key={key} />

describe('<StageContainer />', () => {
    let component
    let children = []

    beforeEach(() => {
        for (let i = 0; i < 10; i++) {
            children.push(div(i))
        }

        component = setUp({}, children)
    })

    afterEach(() => {
        children = []
    })

    it('Should render component', function () {
        expect(component.find('.stage-container')).toHaveLength(1)
    });

    it('Stage nav items count should be equal children length', () => {
        let items = component.find('.stage-nav-item')

        expect(items).toHaveLength(children.length)
    })

    it('Should update state on change stage', () => {
        component.instance().changeStage(3)

        setTimeout(() => {
            expect(component.state('stage')).toBe(3)
        }, 300)
    })

    it('Should NOT update state on change stage if new stage greater than children length', () => {
        let stageBeforeUpdate = component.state('stage')

        component.instance().changeStage(children.length + 1)

        expect(component.state('stage')).toBe(stageBeforeUpdate)
    })
})