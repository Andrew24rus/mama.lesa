import React from "react"
import PropTypes from 'prop-types'

class Form extends React.Component {
    static defaultProps = {
        method: 'get',
        action: ''
    }

    constructor(props) {
        super(props)

        this.formRef = React.createRef()
    }

    validate() {
        const required = this.formRef.current.querySelectorAll('[required]')

        let isFormValid = true

        required.forEach(input => {
            let valid = input.inputmask ? input.inputmask.isComplete() : input.checkValidity()

            if (input.inputmask) {
                input.setCustomValidity(valid ? '' : 'error')
            }

            if (!valid) {
                isFormValid = false
            }

            input.closest('.form-group').classList.toggle('is-invalid', !valid)
        })

        this.formRef.current.classList.add('was-validated')

        return isFormValid
    }

    submit() {
        if (this.validate()) {
            this.props.onSubmit?.()
        }
    }

    renderChildren() {
        const { children } = this.props

        const render = (el, i) =>
            <div className="form-group" key={i}>
                { el }

                {el?.props?.required &&
                    <div className="invalid-feedback">
                        { el.props.invalidFeedback || 'Заполните это поле'}
                    </div>
                }
            </div>

        if (Array.isArray(children)) {
            return children.map(render)
        }

        return render(children)
    }

    render() {
        const { className, action, method } = this.props

        return (
            <form action={action}
                  method={method}
                  className={`form ${className ?? ''}`}
                  ref={this.formRef}
                  noValidate
            >
                { this.renderChildren() }
            </form>
        )
    }
}

Form.propTypes = {
    className: PropTypes.string,
    method: PropTypes.oneOf([
        'get', 'post'
    ]),
    action: PropTypes.string,
    onSubmit: PropTypes.func
}

export default Form