import _Picker from "./_Picker"
import React from "react";

class ItemPicker extends _Picker {
    renderItem(item) {
        const { name, required } = this.props
        const selected = this.isSelected(item.value)

        return (
            <label className={`picker-item btn ${selected ? 'active' : ''}`}
                   key={item.value}
            >
                <input {...{name, required}}
                       type={this.getType()}
                       checked={selected}
                       value={item.value}
                       onChange={this.onChange}
                />
                { item.label || item.value }
            </label>
        )
    }

    render() {
        const { items, className, id } = this.props

        return (
            <div id={id} className={`picker ${className}`}>
                <div className="btn-group btn-group-toggle" data-toggle="buttons">
                    {items.map(item => this.renderItem(item))}
                </div>
            </div>
        )
    }
}

export default ItemPicker