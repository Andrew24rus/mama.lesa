import _Control from "./_Control"
import PropTypes from 'prop-types'

class _Input extends _Control
{
    static defaultProps = {
        ..._Control.defaultProps,

        value: '',
        onInput: () => {}
    }

    constructor(props) {
        super(props)

        this.state = {
            value: this.props.value
        }

        this.handleInput = this.handleInput.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    setValue(value) {
        this.setState({ value })
    }

    handleInput({target}) {
        const { value } = target

        this.setValue(value)

        this.props.onInput(value)
    }

    handleChange({target}) {
        const { value } = target

        this.setValue(value)

        this.props.onChange(value)
    }
}

_Input.propTypes = {
    ..._Control.propTypes,

    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onInput: PropTypes.func
}

export default _Input