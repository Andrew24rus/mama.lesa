import React from "react"
import PropTypes from 'prop-types'

class _Control extends React.Component {
    static defaultProps = {
        className: '',
        required: false,
        disabled: false,
        readOnly: false,
        onChange: () => {}
    }
}

_Control.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readOnly: PropTypes.bool,
    tabIndex: PropTypes.number,
    onChange: PropTypes.func,
    invalidFeedback: PropTypes.string
}

export default _Control