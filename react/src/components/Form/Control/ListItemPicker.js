import React from "react";
import PropTypes from "prop-types";
import ItemPicker from "./ItemPicker";

class ListItemPicker extends ItemPicker {
    renderItem(item) {
        return (
            <div className="picker-list-item" key={item.value}>
                {item.image &&
                    <img className="picker-item-image" src={item.image} alt={item.value} />
                }

                {super.renderItem(item)}
            </div>
        )
    }

    render() {
        const { items, className, id } = this.props

        return (
            <div id={id} className={`picker picker-list ${className}`}>
                <div className="btn-group btn-group-toggle" data-toggle="buttons">
                    {items.map(item => this.renderItem(item))}
                </div>
            </div>
        )
    }
}

ListItemPicker.propTypes = {
    ...ItemPicker.propTypes,

    items: PropTypes.arrayOf(PropTypes.shape({
            value: PropTypes.oneOfType([
                PropTypes.string, PropTypes.number
            ]).isRequired,
            label: PropTypes.string,
            image: PropTypes.string
        })
    ).isRequired
}

export default ListItemPicker