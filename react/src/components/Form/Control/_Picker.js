import PropTypes from 'prop-types'
import _Control from "./_Control";

class _Picker extends _Control {
    static defaultProps = {
        ..._Control.defaultProps,

        multiple: false,
        invalidFeedback: 'Выберите один из элементов списка'
    }

    constructor(props) {
        super(props)

        this.state = {
            selected: this.props.selected
        }

        this.onChange = this.onChange.bind(this)
        this.isSelected = this.isSelected.bind(this)
    }

    getType() {
        const { multiple } = this.props

        return multiple ? 'checkbox' : 'radio'
    }

    isSelected(value) {
        const { selected } = this.state

        if (Array.isArray(selected)) {
            return selected.indexOf(value.toString()) !== -1
        }

        return value.toString() === selected
    }

    onChange({target}) {
        const { multiple, onChange } = this.props
        const { selected } = this.state
        const { value } = target
        let newSelected = selected

        if (multiple) {
            if (!Array.isArray(selected)) {
                newSelected = selected ? [selected] : []
            }

            if (this.isSelected(value)) {
                newSelected = newSelected.filter(val => val !== value)
            } else {
                newSelected.push(value)
            }
        } else {
            newSelected = this.isSelected(value) ? null : value
        }

        this.setState({
            selected: newSelected
        })

        onChange(newSelected)
    }
}

_Picker.propTypes = {
    ..._Control.propTypes,

    items: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.oneOfType([
                PropTypes.string, PropTypes.number
            ]).isRequired,
            label: PropTypes.string
        })
    ).isRequired,
    selected: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.arrayOf(
            PropTypes.oneOfType([
                PropTypes.string, PropTypes.number
            ])
        )
    ]),
    multiple: PropTypes.bool,
    onChange: PropTypes.func,
}

export default _Picker