import React from "react"
import PropTypes from 'prop-types'
import _Input from "./_Input";

/* Assets */
require('./../../../assets/js/rSlider')

class Range extends _Input {
    static defaultProps = {
        ..._Input.defaultProps,
        min: 1,
        max: 10,
        step: 1,
        options: {
            tooltip: false,
            labels: true
        }
    }

    constructor(props) {
        super(props)

        this.state = {
            value: this.props.value ?? this.props.min
        }

        this.rangeRef = React.createRef()
    }

    componentDidMount() {
        const { step, value, options } = this.props

        new window.rSlider({
            ...options,
            step,
            scale: false,
            values: this.getRange(),
            target: this.rangeRef.current,
            set: [value],
            onChange: value => this.handleChange({target: { value }})
        })
    }

    getRange() {
        const { step, min, max } = this.props

        let range = []

        for(let i = min; i <= max; i+=step) {
            range.push(i)
        }

        return range
    }

    render() {
        const { className, min, max, step, disabled, readOnly } = this.props
        const { value } = this.state

        return (
            <div className={'range'}>
                <input {...{min, max, step, disabled, readOnly}}
                       value={value}
                       type="range"
                       ref={this.rangeRef}
                       className={`slider ${className}`}
                       //onChange={this.handleChange}

                />
            </div>
        )
    }
}

Range.propTypes = {
    ..._Input.propTypes,

    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    options: PropTypes.shape({
        tooltip: PropTypes.bool,
        tooltipSuffix: PropTypes.string,
        tooltipLabels: PropTypes.object,
        labels: PropTypes.bool
    })
}

export default Range