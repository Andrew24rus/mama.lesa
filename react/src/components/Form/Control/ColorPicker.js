import React from "react"
import PropTypes from 'prop-types'
import ItemPicker from "./ItemPicker"

class ColorPicker extends ItemPicker {
    renderItem(item) {
        const { required, name, disabled, readOnly } = this.props
        const selected = this.isSelected(item.value)

        return (
            <label className={`picker-item picker-item-color ${selected ? 'active' : ''}`}
                   style={{background: item.hex}}
                   key={item.value}
            >
                <input {...{name, required, disabled, readOnly}}
                       type={this.getType()}
                       value={item.value}
                       onChange={this.onChange}
                       checked={selected}
                />
            </label>
        )
    }

    render() {
        const { items, className, id } = this.props

        return (
            <div id={id} className={`picker picker-color ${className}`}>
                <div className="btn-group btn-group-toggle" data-toggle="buttons">
                    {items.map(item => this.renderItem(item))}
                </div>
            </div>
        )
    }
}

ColorPicker.propTypes = {
    ...ItemPicker.propTypes,

    items: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string.isRequired,
        hex: PropTypes.string.isRequired
    })).isRequired
}

export default ColorPicker