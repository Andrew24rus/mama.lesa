import React from "react"
import _Input from "./_Input"
import PropTypes from "prop-types"
import Inputmask from "inputmask/lib/inputmask"

class TextInput extends _Input {
    static defaultProps = {
        ..._Input.defaultProps,

        type: 'text'
    }

    constructor(props) {
        super(props)

        this.inputRef = React.createRef()
    }

    componentDidMount() {
        const { mask } = this.props

        if (mask) {
            Inputmask({ mask }).mask(this.inputRef.current)
        }
    }

    render() {
        const { className, id, name, readOnly, disabled, placeholder, required } = this.props
        const { value } = this.state

        return (
            <input {...{id, name, readOnly, disabled, placeholder, required}}
                   ref={this.inputRef}
                   value={value}
                   className={`form-control ${className}`}
                   onInput={this.handleInput}
                   onChange={this.handleChange}
            />
        )
    }
}

TextInput.propTypes = {
    ..._Input.propTypes,

    type: PropTypes.oneOf([
        'text', 'password', 'number', 'tel', 'email', 'hidden'
    ]),
    mask: PropTypes.string
}

export default TextInput