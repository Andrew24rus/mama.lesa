import React from "react"
import PropTypes from "prop-types"
import _Input from "./_Input"

class TextArea extends _Input {
    render() {
        const { className, value: propValue, ...props } = this.props
        const { value } = this.state

        return (
            <textarea {...props}
                      className={`form-control ${className}`}
                      onInput={this.handleInput}
                      onChange={this.handleChange}
            >
                { value }
            </textarea>
        )
    }
}

TextArea.propTypes = {
    ..._Input.propTypes,

    rows: PropTypes.number,
    cols: PropTypes.number
}

export default TextArea