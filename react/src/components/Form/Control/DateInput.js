import React from "react"
import PropTypes from "prop-types"
import Flatpickr from 'react-flatpickr'
import { Russian } from "flatpickr/dist/l10n/ru.js"
import "flatpickr/dist/flatpickr.min.css"
import _Input from "./_Input"

class DateInput extends _Input {
    static defaultProps = {
        ..._Input.defaultProps,

        options: {}
    }

    constructor(props) {
        super(props)

        this.options = {
            disableMobile: true,
            locale: Russian,
            altInput: true,
            altFormat: 'd.m.Y',
            dateFormat: 'Y-m-d',
            allowInput: true,
            ...this.props.options
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(selectedDates, dateStr, instance) {
        const event = {
            target: instance.input
        }

        super.handleChange(event)
    }

    render() {
        const { className, id, name, required, placeholder, readOnly, disabled } = this.props
        const { value } = this.state

        return (
            <Flatpickr {...{id, name, required, placeholder, readOnly, disabled, value}}
                       options={this.options}
                       className={`form-control ${className}`}
                       onChange={this.handleChange}
            />
        )
    }
}

DateInput.propTypes = {
    ..._Input.propTypes,

    options: PropTypes.object
}

export default DateInput