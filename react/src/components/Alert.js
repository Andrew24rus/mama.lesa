import React from 'react'
import PropTypes from 'prop-types'

class Alert extends React.Component {
    static defaultProps = {
        dismissible: false,
        type: 'danger'
    }

    constructor(props) {
        super(props)

        this.alertRef = React.createRef()
    }

    close() {
        this.alertRef.current.classList.remove('show')

        setTimeout(() => this.props.onClose?.(), 300)
    }

    render() {
        const { message, dismissible, type, children } = this.props

        return (
            <div className={`alert alert-${type} ${dismissible ? 'alert-dismissible fade show' : ''}`}
                 role="alert"
                 ref={this.alertRef}
            >
                {dismissible &&
                    <button type="button" className="close" aria-label="Close" onClick={() => this.close()}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                }

                { message }

                { children }
            </div>
        )
    }
}

Alert.propTypes = {
    message: PropTypes.string.isRequired,
    dismissible: PropTypes.bool,
    onClose: PropTypes.func,
    type: PropTypes.oneOf(['info', 'success', 'warning', 'danger'])
}

export default Alert