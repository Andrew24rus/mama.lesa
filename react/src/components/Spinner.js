import React from "react"
import PropTypes from 'prop-types'

class Spinner extends React.Component {
    render() {
        const { text, className } = this.props

        return (
            <div className={`spinner ${className ?? ''}`}>
                <i className="spinner-icon fas fa-spinner" />

                { text }
            </div>
        )
    }
}

Spinner.propTypes = {
    className: PropTypes.string,
    text: PropTypes.string
}

export default Spinner