import React from 'react'
import PropTypes from 'prop-types'

class StageContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: 0
        }

        this.stageContentRef = React.createRef()
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        const { stage } = this.state
        const { children } = this.props
        const nextStage = nextState.stage
        const maxStage = children.length - 1
        const shouldUpdate = (nextStage < maxStage) || (nextStage === maxStage && stage < nextStage)

        if (nextStage > maxStage) {
            this.setState({ stage })

            return false
        }

        return shouldUpdate
    }

    getCurrentScreen() {
        const { children } = this.props
        const { stage } = this.state

        return children[stage]
    }

    changeStage(stage_index) {
        this.stageContentRef.current?.classList.add('fading')

        setTimeout(() => {
            this.stageContentRef.current?.classList.remove('fading')

            this.setState({
                stage: stage_index
            })
        }, 300)
    }

    goNextStage() {
        const { stage } = this.state

        this.changeStage(stage + 1)
    }

    renderStageNav() {
        const { children } = this.props

        return (
            <ul className="stage-nav">
                {Array.isArray(children) ?
                    children.map((screen, i) => this.renderStageNavItem(i)) :
                    this.renderStageNavItem(1)
                }
            </ul>
        )
    }

    renderStageNavItem(stage_index) {
        const { stage } = this.state
        const completed = stage_index < stage
        const current = stage_index === stage


        return (
            <li key={stage_index}
                className={`stage-nav-item ${completed ? 'completed' : ''} ${current ? 'current' : ''}`}
                {...(completed && {onClick: () => this.changeStage(stage_index)})}
            >
                { stage_index + 1 }
            </li>
        )
    }

    render() {
        const CurrentScreen = this.getCurrentScreen()

        return (
            <div className="row stage-container">
                <div className="col-2 pr-0">
                    {this.renderStageNav()}
                </div>

                <div className="col-10 stage-content" ref={this.stageContentRef}>
                    {React.cloneElement(CurrentScreen, {
                        goNextStage: () => this.goNextStage()
                    })}
                </div>
            </div>
        )
    }
}

StageContainer.propTypes = {
    children: PropTypes.arrayOf(
        PropTypes.element
    ).isRequired
}

export default StageContainer