import React from "react"
import PropTypes from 'prop-types'
import Spinner from './Spinner'

class Button extends React.Component {
    static defaultProps = {
        disabled: false,
        awaiting: false,
        iconPosition: 'right'
    }

    renderIcon(position) {
        const { icon, iconPosition, title, awaiting } = this.props
        const margin = title ? position === 'left' ? 'mr-2' : 'ml-2' : null;

        if (icon && !awaiting && iconPosition === position) {
            return (
                <i className={`fas fa-${icon} ${margin}`} />
            )
        }
    }

    renderContent() {
        const { awaiting, awaitingText, title } = this.props

        if (awaiting) {
            return <Spinner text={awaitingText} className={'flex-row'} />
        } else {
            return (
                <>
                    {this.renderIcon('left')}

                    {title}

                    {this.renderIcon('right')}
                </>
            )
        }
    }

    render() {
        const { className, type, onClick, disabled, awaiting } = this.props

        return (
            <button className={`btn btn-primary ${className ?? ''}`}
                    onClick={onClick}
                    disabled={disabled || awaiting}
                    {...(type && { type })}
            >
                { this.renderContent() }

            </button>
        )
    }
}

Button.propTypes = {
    title: PropTypes.string.isRequired,
    className: PropTypes.string,
    type: PropTypes.oneOf(['button', 'submit']),
    disabled: PropTypes.bool,
    awaiting: PropTypes.bool,
    awaitingText: PropTypes.string,
    icon: PropTypes.string,
    iconPosition: PropTypes.oneOf(['left', 'right'])
}

export default Button