import React from "react"
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
import OrderingResult from "../Screen/OrderingResult"
import OrderingForm from "../Screen/OrderingForm"

class Navigator extends React.Component {
    static defaultProps = {
        initialRoute: ''
    }

    render() {
        const { history, initialRoute } = this.props

        return (
            <Switch>
                <Route exact history={history} path='/ordering' component={OrderingForm}/>
                <Route exact history={history} path='/ordering-result' component={OrderingResult}/>
                <Redirect to={initialRoute} />
            </Switch>
        )
    }
}

Navigator.propTypes = {
    initialRoute: PropTypes.string
}

export default Navigator