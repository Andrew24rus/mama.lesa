import React from 'react'
import StageScreen, { WrapScreenComponent } from "./_StageScreen"
import Form from "../../components/Form/Form"
import { TextInput, DateInput } from "../../components/Form/Control"


class DetailsStageScreen extends StageScreen {
    static defaultProps = {
        title: 'Представьтесь',
    }

    getOrderingMinDate() {
        const { cakeParams: { types = [] } } = this.props

        return types.map(type => type.minOrderingDate).sort()[0]
    }

    renderBody() {
        const { order, goNextStage } = this.props
        const { name, phone, date } = order

        return (
            <Form ref={this.formRef} onSubmit={() => goNextStage()}>
                <TextInput value={name}
                           placeholder={'Как вас зовут?'}
                           required={true}
                           onInput={value => this.handleInput('name', value)}
                           invalidFeedback={'Вы не представились'}
                />

                <TextInput value={phone}
                           type={'tel'}
                           placeholder={'Номер телефона'}
                           required={true}
                           onInput={value => this.handleInput('phone', value)}
                           mask={'+7(999)999-99-99'}
                           invalidFeedback={'Укажите номер телефона для связи'}
                />

                <DateInput value={date}
                           placeholder={'Дата доставки'}
                           required={true}
                           onChange={value => this.handleInput('date', value)}
                           options={{
                               minDate: this.getOrderingMinDate()
                           }}
                           invalidFeedback={'Укажите дату доставки'}
                />
            </Form>
        )
    }
}

export default WrapScreenComponent(DetailsStageScreen)