import React from 'react'
import StageScreen, { WrapScreenComponent } from "./_StageScreen"
import Form from "../../components/Form/Form"
import { ListItemPicker } from "../../components/Form/Control"

class CakeFillingStageScreen extends StageScreen {
    static defaultProps = {
        title: 'Выберите начинку'
    }

    renderBody() {
        const { order, cakeParams, goNextStage } = this.props
        const { filling } = order
        const cakeFillings = cakeParams.fillings

        return (
            <Form ref={this.formRef} onSubmit={goNextStage}>
                <ListItemPicker
                    items={cakeFillings}
                    selected={ filling }
                    name="filling"
                    invalidFeedback={'Выберите начинку'}
                    required
                    onChange={value => this.handleInput('filling', value)}
                />
            </Form>
        )
    }
}

export default WrapScreenComponent(CakeFillingStageScreen)