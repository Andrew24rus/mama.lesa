import React from 'react'
import { connect } from 'react-redux'
import { setOrderData, clearOrderData, sendOrder } from "../../redux/actions/orderActions"
import Button from "../../components/Button"
import Alert from "../../components/Alert"
import {clearAlerts, deleteAlert} from "../../redux/actions/alertActions"

class _StageScreen extends React.Component {
    static defaultProps = {
        title: null
    }

    constructor(props) {
        super(props)

        this.formRef = React.createRef()
        this.handleInput = this.handleInput.bind(this)
    }

    handleInput(key, value) {
        const {setOrderData} = this.props

        setOrderData(key, value)
    }

    onNextBtnClick() {
        this.formRef.current?.submit()
    }

    clearAlerts() {
        this.props.clearAlerts()
    }

    calcAmount() {
        const { cakeParams: {types}, order: {type, weight = 1} } = this.props
        const cakeType = types.find(cakeType => cakeType.value === type)
        const basePrice = cakeType?.price

        return basePrice ? basePrice * weight : null
    }

    renderAlerts() {
        const { alerts, deleteAlert } = this.props

        if (Array.isArray(alerts)) {
            return alerts.map((alert, i) =>
                <Alert message={alert}
                       dismissible={true}
                       onClose={() => deleteAlert(i)}
                />
            )
        }
    }

    renderHeader() {
        const {title} = this.props

        if (title) {
            return (
                <h1 className="title-page">{title}</h1>
            )
        }
    }

    renderFooter() {
        return <Button className={'btn-block'}
                       title={'Дальше'}
                       onClick={() => this.onNextBtnClick()}
                       icon={'arrow-right'}
        />
    }

    renderBody() {
        return <></>
    }

    render() {
        return (
            <>
                {this.renderHeader()}

                {this.renderAlerts()}

                <div className={'stage-body'}>
                    { this.renderBody() }
                </div>

                {this.renderFooter()}
            </>
        )
    }
}

export default _StageScreen

export const WrapScreenComponent = (ScreenComponent) => {
    const mapStateToProps = state => ({
        alerts: state.alerts,
        order: state.order,
        cakeParams: state.cakeParams
    })

    const mapDispatchToProps = {
        setOrderData, clearOrderData, sendOrder, deleteAlert, clearAlerts
    }

    return connect(mapStateToProps, mapDispatchToProps)(ScreenComponent)
}