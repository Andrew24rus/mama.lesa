import React from 'react'
import StageScreen, { WrapScreenComponent } from "./_StageScreen"
import Form from "../../components/Form/Form"
import { ColorPicker } from "../../components/Form/Control"

class CakeColorStageScreen extends StageScreen {
    static defaultProps = {
        title: 'Выберите цвет'
    }

    renderBody() {
        const { order, cakeParams, goNextStage } = this.props
        const { color } = order
        const cakeColors = cakeParams.colors

        return (
            <Form ref={this.formRef} onSubmit={goNextStage}>
                <ColorPicker items={cakeColors}
                             name={'cake-color'}
                             selected={color}
                             onChange={value => this.handleInput('color', value)}
                             invalidFeedback={'Выберите цвет торта'}
                             required
                />
            </Form>
        )
    }
}

export default WrapScreenComponent(CakeColorStageScreen)