import React from 'react'
import StageScreen, { WrapScreenComponent } from "./_StageScreen"
import Form from "../../components/Form/Form"
import Alert from "../../components/Alert"
import { ItemPicker, Range } from "../../components/Form/Control"
import moment from "moment"

class CakeTypeStageScreen extends StageScreen {
    static defaultProps = {
        title: 'Выберите вид торта'
    }

    constructor(props) {
        super(props)

        const { order } = this.props

        this.state = {
            cakeTypeAvailable: order.type ? this.cakeTypeIsAvailable(order.type) : true
        }
    }

    componentDidMount() {
        const { order } = this.props

        if (!order.weight) {
            this.handleInput('weight', 1)
        }
    }

    findCakeType(needleType) {
        const { cakeParams: { types }} = this.props

        return types.find(type => type.value === needleType)
    }

    cakeTypeIsAvailable(type) {
        const { order } = this.props
        const cakeType = this.findCakeType(type)

        return cakeType && (order.date >= cakeType.minOrderingDate)
    }

    onSelectCakeType(value) {
        this.handleInput('type', value)

        this.setState({
            cakeTypeAvailable: this.cakeTypeIsAvailable(value)
        })
    }

    renderCakeTypeWarning() {
        const { order } = this.props
        const cakeType = this.findCakeType(order.type)

        if (cakeType) {
            const minOrderingDate = moment(cakeType.minOrderingDate).format('LL')
            let message = `Извините, доставка выбранного вида торта доступна не ранее ${minOrderingDate}
                Вы можете изменить дату доставки, или выбрать другой вид торта`

            return (
                <Alert message={message} type={'warning'} />
            )
        }
    }

    renderRangeWeight() {
        const { order } = this.props

        return (
            <>
                <h2 className="title-page">
                    <label htmlFor="cakeWeight">Вес торта, кг</label>
                </h2>

                <Range id={'cakeWeight'}
                       value={order.weight}
                       onChange={value => this.handleInput('weight', value)}
                       required={true}
                       min={1}
                       max={5}
                />
            </>
        )
    }

    renderAmount() {
        const amount = this.calcAmount()

        if (amount) {
            return <h2 className="title-page">{ amount } руб.</h2>
        }
    }

    renderBody() {
        const { cakeTypeAvailable } = this.state
        const { order, cakeParams, goNextStage} = this.props

        return (
            <Form ref={this.formRef} onSubmit={() => goNextStage()}>
                <ItemPicker items={cakeParams.types}
                            name={'cake-type'}
                            selected={order.type}
                            required={true}
                            invalidFeedback={'Выберите вид торта'}
                            onChange={value => this.onSelectCakeType(value)}
                />

                { cakeTypeAvailable ?
                    this.renderRangeWeight() :
                    this.renderCakeTypeWarning()
                }

                { cakeTypeAvailable && this.renderAmount() }
            </Form>
        )
    }

    renderFooter() {
        const { cakeTypeAvailable } = this.state

        if (cakeTypeAvailable) {
            return super.renderFooter()
        }
    }
}

export default WrapScreenComponent(CakeTypeStageScreen)