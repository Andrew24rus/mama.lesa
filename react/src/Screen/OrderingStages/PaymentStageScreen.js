import React from 'react'
import StageScreen, { WrapScreenComponent } from "./_StageScreen"
import Button from "../../components/Button"
import Alert from "../../components/Alert"

class PaymentStageScreen extends StageScreen {
    static defaultProps = {
        title: 'Оплата'
    }

    constructor(props) {
        super(props)

        this.state = {
            isSent: false
        }
    }

    sendOrder() {
        const { order, sendOrder } = this.props

        this.clearAlerts()

        sendOrder(order).then(resp => {
            if (resp?.paymentUrl) {
                this.setState({ isSent: true })

                window.location.href = resp.paymentUrl
            }
        })
    }

    renderBody() {
        const { order } = this.props

        return (
            <>
                <Alert
                    type={'info'}
                    message={`
                    Для завершения заказа необходимо внести предоплату в размере 500 руб.
                    Остальную стоимость вы  можете оплатить при получении заказа
                `}/>

                <small>


                </small>

                <table className={'table table-borderless table-striped table-sm'}>
                    <thead>
                    <tr>
                        <th colSpan={2} className={'text-center'}>
                            <b>Информация о заказе</b>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><b>Вид торта</b></td>
                        <td>{order.type}</td>
                    </tr>

                    <tr>
                        <td><b>Вес, кг</b></td>
                        <td>{order.weight}</td>
                    </tr>

                    <tr>
                        <td><b>Начинка</b></td>
                        <td>{order.filling}</td>
                    </tr>

                    <tr>
                        <td><b>Цвет</b></td>
                        <td>{order.color}</td>
                    </tr>
                    <tr>
                        <td><b>Цена</b></td>
                        <td>{this.calcAmount()} руб.</td>
                    </tr>
                    </tbody>
                </table>
            </>
        )
    }

    renderFooter() {
        const { order } = this.props
        const { isSent } = this.state

        return <Button title={'Перейти к оплате'}
                       className={'btn-block'}
                       disabled={isSent}
                       awaiting={order.isSending}
                       awaitingText={'Отправка данных...'}
                       onClick={() => this.sendOrder()}
        />
    }
}

export default WrapScreenComponent(PaymentStageScreen)