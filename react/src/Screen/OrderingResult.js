import React from "react"
import PropTypes from 'prop-types'

class OrderingResult extends React.Component {
    render() {
        return (
            <div className={'row'}>
                <div className="col-12">
                    <h1 className="title-page">Ваш заказ успешно оформлен</h1>
                </div>
            </div>
        )
    }
}

OrderingResult.propTypes = {
    order: PropTypes.object
}

export default OrderingResult