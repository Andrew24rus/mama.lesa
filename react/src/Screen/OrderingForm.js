import React from "react"
import { connect } from "react-redux"
import { fetchParams } from "../redux/actions/paramsActions"
import StageContainer from "../components/StageContainer"
import DetailsStageScreen from "./OrderingStages/DetailsStageScreen"
import CakeTypeStageScreen from "./OrderingStages/CakeTypeStageScreen"
import CakeFillingStageScreen from "./OrderingStages/CakeFillingStageScreen"
import CakeColorStageScreen from "./OrderingStages/CakeColorStageScreen"
import PaymentStageScreen from "./OrderingStages/PaymentStageScreen"
import Spinner from "../components/Spinner"

class OrderingForm extends React.Component {
    componentDidMount() {
        this.props.fetchParams()
    }

    render() {
        const { history, isFetchingParams } = this.props

        return isFetchingParams ?
            <Spinner text={'Загрузка...'} /> :
            <StageContainer>
                <DetailsStageScreen />
                <CakeTypeStageScreen />
                <CakeFillingStageScreen />
                <CakeColorStageScreen />
                <PaymentStageScreen history={history} />
            </StageContainer>
    }
}

const mapStateToProps = state => ({
    isFetchingParams: state.cakeParams.isFetching
})

const mapDispatchToProps = {
    fetchParams
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderingForm)